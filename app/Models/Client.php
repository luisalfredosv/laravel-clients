<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Client extends Model
{

    use HasFactory;
    use SoftDeletes;

    protected $table = "clients";

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
