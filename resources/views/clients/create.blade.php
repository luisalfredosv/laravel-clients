@extends('layouts.app') @section('content')
<div class="container">
    <h2>Crear Cliente</h2>

    @if (Session::has('success'))
    <div class="alert alert-danger">
        {{ Session::get("error") }}
    </div>
    @endif

    <form action="{{ route('clients.store') }}" method="POST">
        @csrf
        <div class="form-group">
            <label for="dni">Cédula</label>
            <input
                type="number"
                min="1000000"
                id="dni"
                name="dni"
                class="form-control"
                required
            />
        </div>

        <div class="form-group">
            <label for="name">Nombres</label>
            <input
                type="text"
                id="name"
                name="name"
                class="form-control"
                required
            />
        </div>

        <div class="form-group">
            <label for="last_name">Apellidos</label>
            <input
                type="text"
                id="last_name"
                name="last_name"
                class="form-control"
                required
            />
        </div>

        <div class="form-group">
            <label for="phone">Teléfono</label>
            <input
                type="text"
                minlength="9"
                id="phone"
                name="phone"
                class="form-control"
                required
            />
        </div>

        <div class="form-group">
            <label for="address">Dirección</label>
            <textarea
                id="address"
                name="address"
                class="form-control"
                required
            ></textarea>
        </div>

        <br />
        <button type="submit" class="btn btn-primary">Crear Cliente</button>
    </form>
</div>

@endsection
