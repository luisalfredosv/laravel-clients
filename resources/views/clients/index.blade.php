@extends('layouts.app') @section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div
                    class="card-header d-flex justify-content-between align-items-center"
                >
                    {{ __("Clientes") }}
                    <a
                        class="btn btn-success"
                        href="{{ route('clients.create') }}"
                    >
                        Crear cliente
                    </a>
                </div>

                @if (session('error'))
                <div class="alert alert-danger">
                    {{ session("error") }}
                </div>
                @endif @if (session('success'))
                <div class="alert alert-success">
                    {{ session("success") }}
                </div>
                @endif

                <br />
                <div class="container">
                    <form action="{{ route('clients.index') }}" method="GET">
                        <div class="input-group mb-3">
                            <input
                                class="input-group-text"
                                type="text"
                                name="search"
                                placeholder="Buscar por cedula"
                                value="{{ old('search', request()->search) }}"
                                autocomplete="off"
                            />
                            <button class="btn btn-info" type="submit">
                                Buscar
                            </button>
                        </div>
                    </form>

                    <table class="table">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">Id</th>
                                <th>
                                    <a
                                        href="{{ route('clients.index', ['sortby' => $sortBy === 'dni_asc' ? 'dni_desc' : 'dni_asc']) }}"
                                    >
                                        Cedula @if (Str::startsWith($sortBy,
                                        'dni')) @if ($sortBy === 'dni_asc')
                                        &uarr; @else &darr; @endif @endif
                                    </a>
                                </th>
                                <th scope="col">Nombres</th>
                                <th scope="col">
                                    <a
                                        href="{{ route('clients.index', ['sortby' => $sortBy === 'last_name_asc' ? 'last_name_desc' : 'last_name_asc']) }}"
                                    >
                                        Apellidos @if (Str::startsWith($sortBy,
                                        'last_name')) @if ($sortBy ===
                                        'last_name_asc') &uarr; @else &darr;
                                        @endif @endif
                                    </a>
                                </th>
                                <th scope="col">Teléfono</th>
                                <th scope="col">Dirección</th>
                                <th scope="col">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($clients as $client)
                            <tr>
                                <td>{{ $client->id }}</td>
                                <td>{{ $client->dni }}</td>
                                <td>{{ $client->name }}</td>
                                <td>{{ $client->last_name }}</td>
                                <td>{{ $client->phone }}</td>
                                <td>{{ $client->address }}</td>

                                <td class="d-flex">
                                    <div>
                                        <a
                                            href="{{ route('clients.edit', $client->id) }}"
                                            class="btn btn-primary"
                                            >Editar</a
                                        >
                                    </div>

                                    <form
                                        action="{{ route('clients.destroy', $client->id) }}"
                                        method="POST"
                                    >
                                        @csrf @method('DELETE')
                                        <button
                                            type="submit"
                                            class="btn btn-danger"
                                        >
                                            Eliminar
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="6">
                                    No hay clientes registrados.
                                </td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @endsection
</div>
