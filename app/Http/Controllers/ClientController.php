<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Client;
use Illuminate\Support\Str;
use Exception;

class ClientController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sortBy = $request->get('sortby', 'dni_asc');
        $search = $request->get('search');

        [$sortField, $sortOrder] = $this->getSortFields($sortBy);

        $query = Client::orderBy($sortField, $sortOrder);

        if (!is_null($search)) {
            $query->where('dni', 'like', "%{$search}%");
        }

        $clients = $query->get();

        return view('clients.index', compact('clients', 'sortBy'));
    }

    protected function getSortFields($sortBy)
    {
        $sortOrder = Str::endsWith($sortBy, 'desc') ? 'desc' : 'asc';
        $sortField = Str::startsWith($sortBy, 'last_name') ? 'last_name' : 'dni';

        return [$sortField, $sortOrder];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('clients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $hasClientSavedForUser = Client::query()
                ->where('dni', $request->input('dni'))
                ->exists();

            if ($hasClientSavedForUser) {
                return redirect()->route('clients.index')->with('error', 'Ya existe un registro con el numero de cédula ingresado');
            }

            $request->validate([
                'dni' => 'required|unique:clients|max:255',
                'name' => 'required',
                'last_name' => 'required',
                'phone' => 'required',
                'address' => 'required',
            ]);

            $client = new Client;
            $client->dni = $request->dni;
            $client->name = $request->name;
            $client->last_name = $request->last_name;
            $client->phone = $request->phone;
            $client->address = $request->address;
            $client->user_id = Auth::id();

            $client->save();

            return redirect()->route('clients.index')->with('success', 'Cliente creado correctamente!');
        } catch (Exception $e) {

            return redirect()->route('clients.index')->with('error', 'Ocurrió un error al crear el cliente');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $client = Client::findOrFail($id);

            return view('clients.show', compact('client'));
        } catch (Exception $e) {

            return redirect()->route('clients.index')->with('error', 'Ocurrió un error al buscar el cliente');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $request->validate([
                'dni' => 'required|max:255|unique:clients,dni,' . $id,
                'name' => 'required',
                'last_name' => 'required',
                'phone' => 'required',
                'address' => 'required',
            ]);

            $client = Client::findOrFail($id);

            $client->dni = $request->dni;
            $client->name = $request->name;
            $client->last_name = $request->last_name;
            $client->phone = $request->phone;
            $client->address = $request->address;

            $client->save();

            return redirect()->route('clients.index')->with('success', 'Cliente actualizado correctamente!');
        } catch (Exception $e) {

            dd($e);
            return redirect()->route('clients.index')
                ->with('error', 'Ocurrió un error al actualizar el cliente');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $client = Client::findOrFail($id);
            $client->delete();

            return redirect()->route('clients.index')
                ->with('success', 'Cliente eliminado correctamente!');
        } catch (Exception $e) {
            return redirect()->route('clients.index')
                ->with('error', 'Ocurrió un error al eliminar el cliente');
        }
    }
}
