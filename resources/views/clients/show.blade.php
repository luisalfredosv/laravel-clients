@extends('layouts.app') @section('content')
<div class="container">
    <h2>Editar Cliente</h2>

    <form action="{{ route('clients.update', $client->id) }}" method="POST">
        @csrf @method('PUT')
        <div class="form-group">
            <label for="dni">Cédula</label>
            <input
                type="number"
                min="1000000"
                id="dni"
                name="dni"
                class="form-control"
                value="{{ old('dni', $client->dni) }}"
                required
            />
        </div>

        <div class="form-group">
            <label for="name">Nombres</label>
            <input
                type="text"
                id="name"
                name="name"
                class="form-control"
                value="{{ old('name', $client->name) }}"
                required
            />
        </div>

        <div class="form-group">
            <label for="last_name">Apellidos</label>
            <input
                type="text"
                id="last_name"
                name="last_name"
                class="form-control"
                value="{{ old('last_name', $client->last_name) }}"
                required
            />
        </div>

        <div class="form-group">
            <label for="phone">Teléfono</label>
            <input
                type="text"
                minlength="9"
                id="phone"
                name="phone"
                class="form-control"
                value="{{ old('phone', $client->phone) }}"
                required
            />
        </div>

        <div class="form-group">
            <label for="address">Dirección</label>
            <textarea
                id="address"
                name="address"
                class="form-control"
                required
                >{{ old('address', $client->address) }}</textarea
            >
        </div>

        <br />
        <button type="submit" class="btn btn-primary">
            Actualizar Cliente
        </button>
    </form>
</div>

@endsection
